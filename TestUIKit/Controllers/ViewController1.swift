//
//  ViewController1.swift
//  TestUIKit
//
//  Created by Muhammad Asad Chattha on 18/07/2023.
//

import UIKit

class ViewController1: UIViewController {
    static let identifier = "VC1"
    var screenDimissClosure : ((ScreenTypes) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func saveBarButtonTapped(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true) {
            self.screenDimissClosure?(.VC2)
        }
    }
    
}
