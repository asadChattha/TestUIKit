//
//  VehicleFeatureTableViewCell.swift
//  RediGo
//
//  Created by Coder Crew on 24/06/2023.
//

import UIKit

class VehicleFeatureTableViewCell: UITableViewCell {
    // MARK: - @IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var edgeConstraint: NSLayoutConstraint!
    @IBOutlet var detailLabel: UILabel!
    
    // MARK: - Properties
    static let identifier = "VehicleFeatureTableViewCell"

    // MARK: - Lifecycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - @IBActions
    @IBAction func buttonTapped(_ sender: UIButton) {
        print("Button Tapped")

        let zeroHeightConstraint = detailLabel.heightAnchor.constraint(equalToConstant: 0)
        edgeConstraint.isActive = false
        zeroHeightConstraint.isActive = true
        
        
        UIView.animate(withDuration: 1, animations: {
            self.edgeConstraint.isActive = false
            zeroHeightConstraint.isActive = true
            self.detailLabel.layoutIfNeeded()
        })

    }
    
}

// MARK: - Methods
extension VehicleFeatureTableViewCell {
    
}
