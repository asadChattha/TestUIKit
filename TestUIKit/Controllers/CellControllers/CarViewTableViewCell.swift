//
//  CarViewTableViewCell.swift
//  TestUIKit
//
//  Created by Muhammad Asad Chattha on 27/06/2023.
//

import UIKit

class CarViewTableViewCell: UITableViewCell {
    // MARK: - @IBOutlets
    
    // MARK: - Properties
    static let identifier = "CarViewTableViewCell"

    // MARK: - Lifecycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - @IBActions

}

// MARK: - Methods
extension CarViewTableViewCell {
    
}

