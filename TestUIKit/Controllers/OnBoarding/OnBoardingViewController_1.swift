//
//  OnBoardingViewController-1.swift
//  RediGo
//
//  Created by Coder Crew on 07/06/2023.
//

import UIKit

class OnBoardingViewController_1: UIViewController {
    static let identifier = "OnBoarding_1"

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // MARK: - Change Status-bar color to dark
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}

