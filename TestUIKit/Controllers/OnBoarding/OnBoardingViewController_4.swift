//
//  OnBoardingViewController_4.swift
//  RediGo
//
//  Created by Coder Crew on 08/06/2023.
//

import UIKit

class OnBoardingViewController_4: UIViewController {
    // MARK: - Constants
    static let identifier = "OnBoarding_4"

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - @IBAction
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        // Set true Users is Not New
        // UserDefaultsManager.shared.setIsNotNewUser()
        
        // Navigate to Main Storyboard
        let mainVC = UIStoryboard.main.instantiateViewController(withIdentifier: "TestVC") as! TestVC
        mainVC.modalPresentationStyle = .fullScreen
        self.present(mainVC, animated: true)
    }
    
    // MARK: - Change Status-bar color to dark
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
    
}
