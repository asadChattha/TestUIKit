//
//  OnBoardingViewController_3.swift
//  RediGo
//
//  Created by Coder Crew on 08/06/2023.
//

import UIKit

class OnBoardingViewController_3: UIViewController {
    // MARK: - Constants
    static let identifier = "OnBoarding_3"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    // MARK: - Change Status-bar color to dark
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }

}
