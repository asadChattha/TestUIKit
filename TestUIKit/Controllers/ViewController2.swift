//
//  ViewController2.swift
//  TestUIKit
//
//  Created by Muhammad Asad Chattha on 18/07/2023.
//

import UIKit

class ViewController2: UIViewController {
    static let identifier = "VC2"

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func popButtonTapped(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
