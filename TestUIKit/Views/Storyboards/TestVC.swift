//
//  TestVC.swift
//  TestUIKit
//
//  Created by Muhammad Asad Chattha on 16/07/2023.
//

import UIKit

class TestVC: UIViewController {
    
    static let identifier = "TestVC"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        guard let vc1 = UIStoryboard.main.instantiateViewController(withIdentifier: "VC1") as? ViewController1 else {return}
        
        vc1.screenDimissClosure = { type in
            self.screenFlow(type: .VC2)
        }
        present(vc1, animated: true)
    }
}

extension TestVC {
    private func screenFlow(type: ScreenTypes) {
        switch type {
        case .VC1:
            break
        case .VC2:
            let vc = UIStoryboard.main.instantiateViewController(withIdentifier: ViewController2.identifier)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

