//
//  ScreenTypes.swift
//  TestUIKit
//
//  Created by Muhammad Asad Chattha on 18/07/2023.
//

import Foundation

enum ScreenTypes {
    case VC1
    case VC2
}
